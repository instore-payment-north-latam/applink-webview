import React, { Component } from 'react'

import Button from '@vtex/styleguide/lib/Button';

export default class vtexSettings extends Component {

   constructor(props) {
      super(props);
      this.state = {
         dataAppKey: '',
         dataAppToken: '',
         infoDevice: ''
      };
  
      this.handleChange = this.handleChange.bind(this);
      this.setData = this.setData.bind(this);
   }

   componentDidMount() {
      this.setState({infoDevice: window.navigator.userAgent})
      this.handleLoad();
      this.setData();
   }

   handleLoad(){
      console.log('handleLoad');
      let _this = this;

      document.addEventListener("message", function(event) {
         var parsedData = JSON.parse(event.data);

         _this.setData();
      }, false);
   }

   setData(){
   }
   
   handleChange(event) {
      var targetName = event.target.name;
      var targetValue = event.target.value;
      this.setState({[targetName]: targetValue});
   }

   handleClick() {
      console.log('The link was clicked.');
      var formVtexSettings = $('#form-vtex-settings').serialize();
      ReactNativeWebView.postMessage(formVtexSettings);
   }

   render() {
      return (
         <div className="pv5">
            <form id="form-vtex-settings">
               <div>
                  <div>
                     <label htmlFor="appKey">App key</label>
                  </div>
                  <div>
                  <input type="text" id="appKey" 
                     name="dataAppKey" 
                     value={this.state.dataAppKey} 
                     onChange={this.handleChange}
                   />
                  </div>
               </div>
               <div>
                  <div>
                     <label htmlFor="apptoken">App Token</label>
                  </div>
                  <div>
                  <input type="text" id="apptoken" 
                     name="dataAppToken" 
                     value={this.state.dataAppToken} 
                     onChange={this.handleChange}
                  />
                  </div>
               </div>

               <div>
                  <p className="info-par">- {this.state.infoDevice}</p>
               </div>
               
               <div className="mt5">
                  <Button 
                     onClick={this.handleClick}
                     block>
                     Guardar
                  </Button>
               </div>
            </form>
         </div>
      )
   }
}
