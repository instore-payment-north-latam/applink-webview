import React, { Component } from 'react';
import Button from '@vtex/styleguide/lib/Button';
import Card from '@vtex/styleguide/lib/Card';
import Spinner from '@vtex/styleguide/lib/Spinner';

let dataPaymentType, dataRegistryModel, allData;
export default class scannerScreen extends Component {
   constructor(props) {
      super(props);
      this.state = {
         test: 'Init',
         showInfo: false
      };

      this.setData = this.setData.bind(this);
   }

   componentDidMount() {
      console.log('YES')
      this.handleLoad();
   }

   handleLoad(){
      let _this = this;

      document.addEventListener("message", function(event) {
         let parsedData = JSON.parse(event.data);
         dataPaymentType = parsedData['transactionPaymentType'];
         dataRegistryModel = parsedData['registryModel'];
         allData = {
            'transactionDeviceId': parsedData['transactionDeviceId'], 
            'transactionPaymentType': parsedData['transactionPaymentType'], 
            'registryModel': parsedData['registryModel'],
            'sellerID': parsedData['sellerID'], 
            'createdAt': parsedData['createdAt']
         }
      
         _this.setData();
      }, false);
   }

   setData(){
      if (dataPaymentType && dataRegistryModel) {
         this.setState({showInfo: true})
      }
      this.setState({test: dataRegistryModel})
   }

   retryScanner(){
      ReactNativeWebView.postMessage('retryScanner');
   }

   render() {
    return (

      <div>
         <div className="content-scann">
            <div style={{ padding: '20px', color: '#585959', background: '#fafafa', width: '100%' }}>
            { this.state.showInfo && 
               <div>
                  <h1 className="t-heading-2 mb8 tc">Dispositivo Registrado</h1>
                  <Card>
                     <h3>Información General</h3>
                     <p>ID: {allData.transactionDeviceId}</p>
                     <p>Seller: {allData.sellerID}</p>
                     <p>Fecha de creación: {allData.createdAt}</p>
                     <p>Modelo: {allData.registryModel}</p>
                  </Card>
               </div>
            }

            { !this.state.showInfo && 
               <div className="tc">
                  <Card>
                  <h1 className="t-heading-2 mb8">Validando scanner</h1>
                  
                  <Spinner size={40} />

                  <div className="mv7">
                     <Button variation="primary" size="large" onClick={this.retryScanner}>
                        Volver a Scannear
                     </Button>
                  </div>
                  </Card>
               </div>
            } 
            </div>
         </div>
         {/* <p className="pa7">Log: { this.state.test }</p>   */}
      </div>
    )
  }
}
