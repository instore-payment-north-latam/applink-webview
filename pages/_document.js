import Document, { Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  static getInitialProps(ctx) {
    return Document.getInitialProps(ctx)
  }

  render() {
    return (
      <html>
        <Head>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="icon" href="/img/favicon.ico" type="image/x-icon" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script type="text/javascript" src="/js/jquery.js"></script>
          <script type="text/javascript" src="/js/global.js"></script>
        </body>
      </html>
    )
  }
}
