import React, { Component } from 'react';
import LayoutSect from '@vtex/styleguide/lib/Layout';
import PageBlock from '@vtex/styleguide/lib/PageBlock';
import PageHeader from '@vtex/styleguide/lib/PageHeader';
import CaretLeft from '@vtex/styleguide/lib/icon/CaretLeft';
import Deny from '@vtex/styleguide/lib/icon/Deny';
import Success from '@vtex/styleguide/lib/icon/Success';
import ButtonWithIcon from '@vtex/styleguide/lib/ButtonWithIcon';
import Link from 'next/link';


export default class approvedPay extends Component {

   constructor(props) {
      super(props)
      this.state = {}
   }

   _onClick(value) {
      console.log(value)
      try {
         ReactNativeWebView.postMessage(value);
      } catch (error) {}
   }
   
   render() {
      return (
         <div>
            <div className='pa3'>
               <Link href="/">
                  <ButtonWithIcon icon={<CaretLeft />} variation="tertiary" />
               </Link>
            </div>
            
            <LayoutSect pageHeader={<PageHeader title="Pago Aprobado" />}>
               <PageBlock>
                  <div className="flex justify-between pa5">
                     <ButtonWithIcon icon={<Deny />} variation="danger"
                     onClick={() => this._onClick('anular')}>
                        Anular
                     </ButtonWithIcon>
                     <ButtonWithIcon icon={<Success />} iconPosition="right"
                     onClick={() => this._onClick('continuar')}>
                        Continuar
                     </ButtonWithIcon>
                  </div>
               </PageBlock>
            </LayoutSect>
         </div>
      )
   }
}
