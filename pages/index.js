import React, { Component } from 'react';
import Layout from '../components/MyLayout.js'
import Link from 'next/link';
import Button from '@vtex/styleguide/lib/Button';
import Cog from '@vtex/styleguide/lib/icon/Cog';
import ButtonWithIcon from '@vtex/styleguide/lib/ButtonWithIcon';
import Alert from '@vtex/styleguide/lib/Alert';
import Columns from '@vtex/styleguide/lib/icon/Columns';
import queryString from 'query-string';
import Router from 'next/router'
import Spinner from '@vtex/styleguide/lib/Spinner'
// function PostLink(props) {
//   return (
//     <li>
//       <Link href={`/p/${props.id}`}>
//         <a>{props.title}</a>
//       </Link>
//     </li>
//   )
// }


import {
  osVersion,
  isAndroid
} from "react-device-detect";

let togglePayRedeban, togglePayMercado, togglePayScanner, allData, dataStoreName, showOptionalPayment, makeTransactionLane, registryModel;
let openScreen;
export default class index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      log: '...',
      customtext: '',
      showAlert: false,
      adroid5: false,
      redirectScreen: false,
      infoDevice: ''
    };
    
    this.setData = this.setData.bind(this);
  }

  componentDidMount() {
    const parsed = queryString.parse(location.search);
    if (parsed.screen) {
      this.setState({redirectScreen: true})
      if (parsed.screen=='scanner') {
        Router.push('/scannerScreen')
      }
      if (parsed.screen=='paytransaction') {
        Router.push('/payTransaction')
      }
    }

    this.setState({infoDevice: window.navigator.userAgent})

    // console.log('componentDidMount', osVersion, isAndroid);
    // if (isAndroid && osVersion.includes('5.1')) {
    //   this.setState({infoDevice: window.navigator.userAgent})

    //   this.setState({adroid5: true})
    // } else {
    //   this.setState({infoDevice: window.navigator.userAgent})
    // }

    this.handleLoad();
  }

  handleLoad(){
      console.log('handleLoad');
      let _this = this;

      document.addEventListener("message", function(event) {
        // var parsedData = event.data;
        // allData = parsedData;
        var parsedData = JSON.parse(event.data);
        
        makeTransactionLane = parsedData['makeTransactionLane'];
        if (makeTransactionLane == 'make') {
          $('#btnSelectTransaction').click();
        }

        openScreen = parsedData['openScreen'];

        if (openScreen == 'sacannerScreen') {
          $('#btnScannerScreen').click();
        }

        showOptionalPayment = parsedData['showOptionalPayment'];
        if (showOptionalPayment == 'redirect') {
          $('#btnSelectPay').click();
        }
        dataStoreName = parsedData['dataNameStore'];
        registryModel = parsedData['registryModel'];

        _this.setData();
      }, false);
  }

  setData(){
    this.setState({customtext: dataStoreName});
    
    if (registryModel) {
      this.setState({showAlert: false});
    } else {
      this.setState({showAlert: true});
    }
    
    this.setState({log: openScreen})
    // if (dataStoreName) {
    //   this.setState({showAlert: false});
    // } else {
    //   this.setState({showAlert: true});
    // }
  }

  handleChange(event) {
    console.log('change')
  }

  clickBtn(value) {
      if (value == 'redeban') {
         value = valueModel;
      }
      try {
         ReactNativeWebView.postMessage(value);   
      } catch (error) {}
   }

  render() {
    return (
      <div>
        { this.state.redirectScreen 
          ? 
            <div className="bg-muted-5 pa5 vh-100 flex justify-center items-center">
                <div className="tc">
                  <h5 className="t-heading-5">Cargando información</h5>
                  <Spinner />
                </div>
            </div>
          : 
            <div>
              <div className='pa3'>

                <ButtonWithIcon icon={<Cog />} variation="tertiary" onClick={() => this.clickBtn('scanner')} />


                <Link href="/selectPay">
                  <ButtonWithIcon id="btnSelectPay" icon={<Columns />} variation="tertiary" />
                </Link>

                {/* <Link href="/payTransaction">
                  <ButtonWithIcon id="btnSelectTransaction" icon={<Columns />} variation="tertiary" />
                </Link> */}

                <Link href="/scannerScreen">
                  <ButtonWithIcon id="btnScannerScreen" icon={<Columns />} variation="tertiary" />
                </Link>

                </div>
                { this.state.showAlert &&
                  <div className="mb5">
                    <Link href="/settings">
                      <Alert type="warning">Es necesario una configuración inicial</Alert>
                    </Link>
                  </div>
                }


                <div className='content-home'>
                  <figure>
                    <img className="img-fluid" src="/img/logo.jpg" alt="Logo"/>
                  </figure>
                  <Link href="/settings">
                    <p className="f7 pa5 pointer">Development</p>
                  </Link>

                  {/* <a href="redeban://payment?screen=PrintScreen&items[0][uniqueId]=D67072669F5E4EAA9C4DE9B85599B026&items[0][id]=74&items[0][productId]=71&items[0][quantity]=1&items[0][seller]=1&items[0][name]=TV Full HD W650D | Sony Tv full HD&items[0][price]=120000000&items[0][listPrice]=120000000&items[0][priceValidUntil]=&items[0][commission]=0&items[0][tax]=0&items[0][unitMultiplier]=1&items[0][sellingPrice]=120000000&items[0][costPrice]=120000000&items[1][uniqueId]=C9BD66B0574A4E5F86CD402D3F671168&items[1][id]=37&items[1][productId]=36&items[1][quantity]=1&items[1][seller]=1&items[1][name]=Camiseta VTEX&items[1][price]=100000&items[1][listPrice]=100000&items[1][priceValidUntil]=&items[1][commission]=0&items[1][tax]=0&items[1][unitMultiplier]=1&items[1][sellingPrice]=100000&items[1][costPrice]=100000&marketplace[name]=newbusinessunits&totals[value]=120100000&clientProfileData[id]=clientProfileData&clientProfileData[email]=0d757e3f783d406cb910ea9f9d94d577@ct.vtex.com.br&clientProfileData[firstName]=isAnonymous&clientProfileData[lastName]=&clientProfileData[documentType]=&clientProfileData[document]=&clientProfileData[phone]=&paymentData[transactions][0][payments][0][paymentSystemName]=Efectivo&paymentData[transactions][0][payments][0][value]=120100000&callCenterOperatorData[userName]=Instore Vendedor 4">Imprimir Factura.</a> */}
                </div>
            </div>
        }
        
      </div>
    )
  }
}
