
import React, { Component } from 'react';
import Layout from '@vtex/styleguide/lib/Layout';
import PageBlock from '@vtex/styleguide/lib/PageBlock';
import PageHeader from '@vtex/styleguide/lib/PageHeader';
import Density from '@vtex/styleguide/lib/icon/Density';
import Copy from '@vtex/styleguide/lib/icon/Copy';
import ButtonWithIcon from '@vtex/styleguide/lib/ButtonWithIcon';
import CaretLeft from '@vtex/styleguide/lib/icon/CaretLeft';
import Cog from '@vtex/styleguide/lib/icon/Cog';
import Link from 'next/link';

export default class readQr extends Component {
   render() {
      return (
         <div>
            <div className='pa3'>
     
               <Link href="/selectPay">
                  <ButtonWithIcon icon={<CaretLeft />} variation="tertiary" />
               </Link>
            </div>
            
            <Layout pageHeader={
               <PageHeader 
                  title="_"
                  subtitle="Lee el siguiente código QR"/>
               }>
               <PageBlock>
                  <div className="flex flex-column w-100 m-auto tc">
                     <figure>
                        <img className="img-fluid" src="/img/qrcode.png" alt="Logo"/>
                     </figure>
                  </div>
               </PageBlock>
            </Layout>
         </div>
      )
   }
}
