import React, { Component } from 'react';
import Layout from '@vtex/styleguide/lib/Layout';
import PageBlock from '@vtex/styleguide/lib/PageBlock';
import PageHeader from '@vtex/styleguide/lib/PageHeader';
import Density from '@vtex/styleguide/lib/icon/Density';
import Copy from '@vtex/styleguide/lib/icon/Copy';
import ButtonWithIcon from '@vtex/styleguide/lib/ButtonWithIcon';
import CaretLeft from '@vtex/styleguide/lib/icon/CaretLeft';
import Cog from '@vtex/styleguide/lib/icon/Cog';
import Link from 'next/link';

let allData;
let togglePayRedeban, togglePayMercado, togglePayScanner, valueModel;
// variation="secondary"
export default class selectPay extends Component {
   constructor() {
      super()
      this.state = {
         customtext: '',
         showRb: false,
         showMp: false,
         showSc: false
      }
      // this.handleTabChange = this.handleTabChange.bind(this)
      this.setData = this.setData.bind(this);
   }

   componentDidMount(){
      this.handleLoad();

      try {
         ReactNativeWebView.postMessage('loadData');   
      } catch (error) {}
   }

   handleLoad = async => {
      let _this = this;

      document.addEventListener("message", function(event) {
         var parsedData = event.data;
         allData = parsedData;
         var parsedData = JSON.parse(event.data);
         togglePayRedeban = parsedData['paySettings']['togglePayRedeban'];
         togglePayScanner=parsedData['paySettings']['togglePayScanner'];
         togglePayMercado=parsedData['paySettings']['togglePayMercado'];
         togglePayMercado=parsedData['paySettings']['togglePayMercado'];

         valueModel=parsedData['paySettings']['valueModel'];
         
        _this.setData();
      }, false);
   }

   setData(){
      if (togglePayRedeban=='on') {
         this.setState({showRb: true});   
      }
      
      if (togglePayScanner=='on') {
         this.setState({showSc: true});   
      }

      if (togglePayMercado=='on') {
         this.setState({showMp: true});   
      }
      this.setState({customtext: allData});
   }

   clickBtn(value) {
      if (value == 'redeban') {
         value = valueModel;
      }
      try {
         ReactNativeWebView.postMessage(value);   
      } catch (error) {}
   }

   render() {
      return (
         <div>
            {/* <textarea 
            value={this.state.customtext}
            onChange={this.handleChange}></textarea> */}
            
            <div className='pa3'>
               <div className="flex justify-between">
                  <Link href="/">
                     <ButtonWithIcon icon={<CaretLeft />} variation="tertiary" />
                  </Link>
                  <Link href="/settings">
                     <ButtonWithIcon icon={<Cog />} variation="tertiary" />
                  </Link>
               </div>
            </div>
            
            <Layout pageHeader={
               <PageHeader 
                  subtitle="Seleccione en el método de pago con el que desea finalizar la transacción"
                  title="Seleccionar método de pago" />
               }>
               <PageBlock>
                  <div className="flex flex-column w-100 m-auto tc">
                     <div className="flex flex-column justify-between">

                        <div className="mt3 w100">
                           <Link href="/readQr">
                              <ButtonWithIcon 
                                 size="large"
                                 onClick={() => console.log('this')}>
                                 Código QR
                              </ButtonWithIcon>
                           </Link>
                        </div>

                        { this.state.showSc &&
                           <div className="mt3 w100">
                              <ButtonWithIcon 
                                 size="large"
                                 iconPosition="right"
                                 onClick={() => this.clickBtn('scanner')}>
                                 Scanner
                              </ButtonWithIcon>
                           </div>
                        }


                        { this.state.showRb &&
                           <div className="mt3">
                              <ButtonWithIcon 
                                 size="large"
                                 onClick={() => this.clickBtn('redeban')}>
                                 Redeban {valueModel}
                              </ButtonWithIcon>
                           </div>
                        }

                        { this.state.showMp &&
                           <div className="mt3">
                              <ButtonWithIcon 
                                 size="large"
                                 onClick={() => this.clickBtn('redeban')}>
                                 Mercado Pago
                              </ButtonWithIcon>
                              </div>
                        }
                     </div>
                  </div>
               </PageBlock>
            </Layout>
         </div>
      )
   }
}
