import React, { Component } from 'react'
import GeneralSettings from './generalSettings';
import VtexSettings from './vtexSettings';
import PaySettings from './paySettings';
import LayoutSect from '@vtex/styleguide/lib/Layout';
import PageBlock from '@vtex/styleguide/lib/PageBlock';
import PageHeader from '@vtex/styleguide/lib/PageHeader';
import Tabs from '@vtex/styleguide/lib/Tabs';
import Tab from '@vtex/styleguide/lib/Tabs/Tab';
import Layout from '../components/MyLayout.js'
import CaretLeft from '@vtex/styleguide/lib/icon/CaretLeft';
import ButtonWithIcon from '@vtex/styleguide/lib/ButtonWithIcon';
import Columns from '@vtex/styleguide/lib/icon/Columns';
import Link from 'next/link';
import Copy from '@vtex/styleguide/lib/icon/Copy';
import { Devices, AccountTree } from '@material-ui/icons';

export default class formSettings extends Component {
   constructor() {
      super()
      this.state = {
        currentTab: 1
      }
      this.handleTabChange = this.handleTabChange.bind(this)
   }

   componentDidMount(){
      try {
         ReactNativeWebView.postMessage('loadData');
      } catch (error) {}
   }
  
   handleTabChange(tabIndex) {
      this.setState({
         currentTab: tabIndex,
      });
      try {
         ReactNativeWebView.postMessage('loadData');   
      } catch (error) {}
      
   }

   clickBtn(value) {
      if (value == 'redeban') {
         value = valueModel;
      }
      try {
         ReactNativeWebView.postMessage(value);   
      } catch (error) {}
   }

   render() {
   return (
      <div>
         <div className='pa3'>
            <div className="flex justify-between">
               <Link href="/">
                  <ButtonWithIcon icon={<CaretLeft />} variation="tertiary" />
               </Link>
               
               <div className="flex">
                  <Devices className="pa2 mh2 general-btn" color="primary" fontSize="large" onClick={() => this.clickBtn('scanner')} />
                  <Link href="/selectPay">
                     <AccountTree className="pa2 mh2 general-btn" color="primary" fontSize="large" />
                  </Link>
               </div>
            </div>
         </div>
         
         <LayoutSect pageHeader={<PageHeader title="Configuración" />}>
            <PageBlock>
               <div className="content-buttons">
                  <Tabs fullWidth>
                     <Tab
                        label="General"
                        active={this.state.currentTab === 1}
                        onClick={() => this.handleTabChange(1)}>
                        <GeneralSettings />
                     </Tab>
                     <Tab
                        label="VTEX"
                        active={this.state.currentTab === 2}
                        onClick={() => this.handleTabChange(2)}>
                        <VtexSettings />
                     </Tab>
                     <Tab
                        label="Medios de pago"
                        active={this.state.currentTab === 3}
                        onClick={() => this.handleTabChange(3)}>
                        <PaySettings />
                     </Tab>
                  </Tabs>
               </div>
            </PageBlock>
         </LayoutSect>
      </div>
   );
   }
}
