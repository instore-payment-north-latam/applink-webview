import React, { Component } from 'react';
import axios from 'axios';
import Button from '@vtex/styleguide/lib/Button';
import * as UrlNode from 'url';

let dataStoreName, codeID;

export default class payTransaction extends Component {

  constructor(props) {
    super(props);
    this.state = {
      customtext: '',
      test: 'Init',
      transactionError: false,
      transactionSucess: false,
      startTransaction: false,
    };

    this.setData = this.setData.bind(this);
    this.createTransaction = this.createTransaction.bind(this);
  }

   componentDidMount() {
      ReactNativeWebView.postMessage('initTransaction');
      this.handleLoad();
   }

   handleLoad(){
      let _this = this;

      document.addEventListener("message", function(event) {
         // let parsedData = event.data;
         let parsedData = JSON.parse(event.data);
         // dataStoreName = parsedData['infoUrl'];

         if (parsedData['infoUrl'] && parsedData['infoDeviceId']) {
            _this.createTransaction(parsedData['infoUrl'], parsedData['infoDeviceId'])
         }
         
         // dataStoreName = JSON.stringify(parsedData);
         // _this.setData();
      }, false);
   }

   setData(){
      this.setState({test: 'setData'});
   }

   createTransaction(url, transactionDeviceId) {
      this.setState({transactionError: false})

      const urlParams = UrlNode.parse(url, true);
      let payerIdentification = '3243'
      if (urlParams.query.payerIdentification) {
         payerIdentification = urlParams.query.payerIdentification
      }

      this.setState({test: payerIdentification})
      let infoTransaction = {
         "orderId": urlParams.query.orderGroupId,
         "paymentId": urlParams.query.paymentId,
         "transactionId": urlParams.query.transactionId,
         "deviceId": transactionDeviceId,
         "paymentType": "webservice-redeban-pay",
         "payerEmail": urlParams.query.payerEmail,
         "payerIdentification": payerIdentification, 
         "amount": urlParams.query.value / 100,
         "taxRate": 0.19, 
         "debugMode": true
      }
   
      let config = {
         method: 'post',
         url: 'https://transactions.vtexnorthlatam.com/api/transactions',
         headers: { 
         'Content-Type': 'application/json'
         },
         data : infoTransaction
      };

      axios(config)
      .then((response) => {
         let data = response.data.data
         if (data._id) {
            codeID = data._id
            
            let useUrl = 'wss://pubsub.vtexnorthlatam.com:9850/sub?id='+codeID
            const ws = new WebSocket(useUrl)
            ws.onmessage = (event) => { 
               console.log('Response WebSocket:', event)
               let data = JSON.parse(event.data)

               if (data.status == "rejected") {
                  this.setState({transactionError: true})
               } else if (data.status == "approved") {
                  this.setState({transactionSucess: true})
               }
               this.setState({test: data.status})
               this.props.onMessage && this.props.onMessage(event) 
            }
            this.setState({startTransaction: true})
            this.setState({ws})
         }
      })
      .catch((error) => {
         this.setState({test: JSON.stringify(error)})
         this.setState({transactionError: true})
      });
   }

   retryTransaction(){   
      ReactNativeWebView.postMessage('retryTransaction');
   }

   cancelTransaction(){
      ReactNativeWebView.postMessage('cancelTransaction');
   }

   approvedTransaction(){   
      ReactNativeWebView.postMessage('approvedTransaction');
   }

   render() {
      return (
      <div className="pa8 vh-100-s flex flex-column items-center-s justify-center-s tc">
         { this.state.startTransaction && !this.state.transactionError && !this.state.transactionSucess &&
            <div className="w-100">
               <figure className="ma0">
                  <img className="img-fluid pay-gif" src="/img/pay.gif" alt="Pay"/>
               </figure>
               <h5 className="t-heading-5">Procesando pago...</h5>
               <Button variation="danger" onClick={this.cancelTransaction} block size="large">Cancelar</Button>
            </div>
         }

         { this.state.transactionError &&  
            <div className="tc">
               <h4 className="t-heading-4 mb8">Error en la transacción</h4>

               <div className="flex">
                  <span className="mb4 mh2">
                     <Button variation="danger" onClick={this.cancelTransaction} block size="large">Cancelar</Button>
                  </span>
                  <span className="mb4 mh2">
                     <Button variation="primary" onClick={this.retryTransaction} block size="large">Reintentar</Button>
                  </span>
               </div>
            </div>
         }

         { this.state.transactionSucess &&  
            <div className="tc">
               <h4 className="t-heading-4 mb8">Pago Exitoso</h4>
               <div className="flex">
                  <span className="mb4 mh2">
                     <Button variation="danger" size="large">Anular</Button>
                  </span>
                  <span className="mb4 mh2">
                     <Button variation="primary" onClick={this.approvedTransaction} size="large">Continuar</Button>
                  </span>
               </div>
            </div>
         }
         {/* <p className="pa7">Log: { this.state.test }</p> */}
      </div>
      )
  }
}
