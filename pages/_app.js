import React from 'react'
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'vtex-tachyons';
import '../public/style/styles.css';



export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}