import React, { Component } from 'react'
import Button from '@vtex/styleguide/lib/Button';
// import Dropdown from '@vtex/styleguide/lib/Dropdown';
import {Dropdown} from 'primereact/dropdown';
import Alert from '@vtex/styleguide/lib/Alert';

const storeSelectItems = [
   {label: 'Totém', value: 'totem'},
   {label: 'Venta asistida', value: 'venta'},
];

const countrySelectItems = [
   { value: 'col', label: 'Colombia' },
   { value: 'ecu', label: 'Ecuador' },
   { value: 'per', label: 'Perú' },
   { value: 'pan', label: 'Panamá' },
];

let customData = '';
let dataNameStore='';
let dataValueType='';
let dataValueCountry='', dataIva;


export default class generalSettings extends Component {

   constructor(props) {
      super(props);
      this.state = {
         dataValueCountry: '',
         dataValueType: '',
         dataNameStore: '',
         dataIva: '',
         textPayment: false,
         missingData: false,
         saveData: false,
      };
  
      this.handleChange = this.handleChange.bind(this);
      this.setData = this.setData.bind(this);
      this.handleClick = this.handleClick.bind(this);
   }

   componentDidMount() {
      console.log('componentDidMount');
      this.handleLoad();
      this.setData(dataNameStore, dataValueType, dataValueCountry);
   }

   handleLoad(){
      console.log('handleLoad');
      let _this = this;

      document.addEventListener("message", function(event) {
         var parsedData = JSON.parse(event.data);

         dataNameStore=parsedData['dataNameStore']; 
         dataValueType=parsedData['dataValueType'];
         dataValueCountry=parsedData['dataValueCountry'];
         dataIva=parsedData['dataIva'];

         _this.setData(dataNameStore, dataValueType, dataValueCountry);
      }, false);
   }

   setData(dataNameStore, dataValueType, dataValueCountry){
      this.setState({dataNameStore: dataNameStore});
      this.setState({dataValueType: dataValueType});
      this.setState({dataValueCountry: dataValueCountry});
      this.setState({dataIva: dataIva});

      if (dataValueCountry == 'col') {
         this.setState({textPayment: false});
         this.enablePay(false);
      } else {
         this.setState({textPayment: true});
         this.enablePay(true);
      }
   }

   handleChange(event) {
      var targetName = event.target.name;
      var targetValue = event.target.value;
      this.setState({[targetName]: targetValue});

      if (targetName=='dataIva') {
         const re = /^[0-9\b]+$/;

         if (targetValue === '' || re.test(targetValue)) {
            this.setState({dataIva: targetValue});
         } else {
            this.setState({dataIva: ''});
         }
      }

      if (targetName=='dataValueCountry') {
         if (targetValue=='col') {
            this.enablePay(false);
         } else {
            this.enablePay(true);
         }  
      }
   }

   enablePay(value) {
      this.setState({textPayment: value});
      $("button:contains('Medios de pago')").attr('disabled', value);
   }

   handleClick() {
      var validateName = this.state.dataNameStore;
      var validateCountry = this.state.dataValueCountry;
      var validateIva = this.state.dataIva;
      if (validateName && validateCountry && validateIva) {
         var formGeneral = $('#form-general').serialize();

         ReactNativeWebView.postMessage(formGeneral);

         this.setState({saveData: true},()=>{
            window.setTimeout(()=>{
              this.setState({saveData:false})
            },3000)
          });
      } else {
         this.setState({missingData: true},()=>{
            window.setTimeout(()=>{
              this.setState({missingData:false})
            },3000)
          });
      }
   }


   render() {
      return (
         <div className="pv5">
         {/* <textarea></textarea> */}

         { this.state.missingData && <Alert type="warning">Los campos con * son necesarios</Alert> }
         { this.state.saveData && <Alert type="success">Información Guardada</Alert>}
         
         <form id="form-general">
            <div>
               <div className="col-25">
                  <label htmlFor="dataName">Nombre de la tienda <span className='heavy-rebel-pink'>*</span></label>
               </div>
               <div className="col-75">
                  <input className="input-name-store" type="text" id="dataName" name="dataNameStore" value={this.state.dataNameStore} onChange={this.handleChange} />
               </div>
            </div>
     
            <div className="mt5 vtex-select-type">
               <label htmlFor="dataValueType">Tipo de Tienda</label>
               <Dropdown 
                  label="Tipo de Tienda"
                  className='custom-select-type w-100 pa2'
                  options={storeSelectItems}
                  value={this.state.dataValueType}
                  onChange={this.handleChange}
                  placeholder="Seleccione tipo de tienda"
                  name="dataValueType"/>
            </div>
           
            <div className="mt5 vtex-select-country">
               <label htmlFor="dataValueCountry">País <span className='heavy-rebel-pink'>*</span></label>
               <Dropdown
                  className='custom-select-country w-100 pa2'
                  placeholder="Seleccione país"
                  options={countrySelectItems}
                  name="dataValueCountry"
                  value={this.state.dataValueCountry}
                  onChange={this.handleChange}
               />
            </div>

            { this.state.textPayment &&
               <p className="f6 rebel-pink">Aún no hay medios de pagos disponibles para este país</p>
            }

            <div>
               <div>
                  <label htmlFor="iva">Iva <span className='heavy-rebel-pink'>*</span></label>
               </div>
               <div>
                  <input type="text" id="iva" 
                     name="dataIva" 
                     value={this.state.dataIva} 
                     onChange={this.handleChange}
                  />
               </div>
            </div>

            <div className="mt5">
               <div className="save-settings">
                  <Button 
                     onClick={this.handleClick}
                     block>
                     Guardar
                  </Button>
               </div>
            </div>
         </form>
         </div>
      )
   }
}
