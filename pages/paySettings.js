import React, { Component } from 'react'
import Box from '@vtex/styleguide/lib/Box';
import Toggle from '@vtex/styleguide/lib/Toggle';
// import Dropdown from '@vtex/styleguide/lib/Dropdown';
import {Dropdown} from 'primereact/dropdown';
import Card from '@vtex/styleguide/lib/Card';
import Checkbox from '@vtex/styleguide/lib/Checkbox'
import Button from '@vtex/styleguide/lib/Button';
import RadioGroup from '@vtex/styleguide/lib/RadioGroup';
import Alert from '@vtex/styleguide/lib/Alert';
import Divider from '@vtex/styleguide/lib/Divider'

const modelSelectItems = [
   { value: 'lane', label: 'Lane 5000 / Move 20500' },
   { value: 'a920', label: 'A920' },
];

let togglePayRedeban, valueModel, dataUrl, dataTerminal, togglePayMercado, dataMpKey, dataMpToken,
togglePayScanner, dataCheckPrint, dataParamTitle, dataParamNit, dataParamAddress, dataParamNameStore, dataParamImage,
saveImageData;
export default class paySettings extends Component {
   constructor(props) {
      super(props);
      this.state = {
         checkedRb: false,
         checkedMp: false,
         checkedSc: false,
         checkPrint: false,
         valueModel: '',
         dataUrl: '',
         dataTerminal: '',
         dataMpKey: '',
         dataMpToken: '',
         dataParamTitle: '',
         dataParamNit: '',
         file: null
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleClick = this.handleClick.bind(this);
   }

   componentDidMount() {
      console.log('componentDidMount');
      this.handleLoad();
   }

   handleLoad(){
      console.log('handleLoad');
      let _this = this;

      document.addEventListener("message", function(event) {
         var parsedData = JSON.parse(event.data);

         togglePayRedeban=parsedData['paySettings']['togglePayRedeban'];
         valueModel=parsedData['paySettings']['valueModel'];
         dataUrl=parsedData['paySettings']['dataUrl'];
         dataTerminal=parsedData['paySettings']['dataTerminal'];
         togglePayMercado=parsedData['paySettings']['togglePayMercado'];
         dataMpKey=parsedData['paySettings']['dataMpKey'];
         dataMpToken=parsedData['paySettings']['dataMpToken'];
         togglePayScanner=parsedData['paySettings']['togglePayScanner'];
         dataCheckPrint=parsedData['paySettings']['dataCheckPrint'];
         dataParamTitle=parsedData['paySettings']['dataParamTitle'];
         dataParamNit=parsedData['paySettings']['dataParamNit'];
         dataParamAddress=parsedData['paySettings']['dataParamAddress'];
         dataParamNameStore=parsedData['paySettings']['dataParamNameStore'];
         dataParamImage=parsedData['paySettings']['dataParamImage'];
         

         _this.setData();
      }, false);
   }
   
   setData(){
      if (togglePayRedeban=='on') {
         this.setState({checkedRb: true});   
      }
      if (togglePayMercado=='on') {
         this.setState({checkedMp: true});   
      }
      if (togglePayScanner=='on') {
         this.setState({checkedSc: true});   
      }
      if (dataCheckPrint=='checked') {
         this.setState({checkPrint: true});   
      }
      this.setState({valueModel: valueModel});
      this.setState({dataUrl: dataUrl});
      this.setState({dataTerminal: dataTerminal});
      this.setState({dataMpKey: dataMpKey});
      this.setState({dataMpToken: dataMpToken});
      this.setState({dataParamTitle: dataParamTitle});
      this.setState({dataParamNit: dataParamNit});
      this.setState({dataParamAddress: dataParamAddress});
      this.setState({dataParamNameStore: dataParamNameStore});
      

      this.setState({
         dataParamImage: decodeURIComponent(dataParamImage)
      });

   }

   handleChange(event) {
      var targetName = event.target.name;
      var targetValue = event.target.value;
      this.setState({[targetName]: targetValue});
   }

   handleClick() {
      var validateRb = this.state.checkedRb;
      var validateModel = this.state.valueModel;

      if (validateRb) {
         if (validateModel) {
            this.successSave();
         } else {
            this.setState({missingData: true},()=>{
               window.setTimeout(()=>{
                 this.setState({missingData:false})
               },3000)
             });
         }
      } else {
         this.successSave();   
      }
   }

   successSave(){
      var formPay = $('#form-pay').serialize();
      ReactNativeWebView.postMessage(formPay);
      this.setState({saveData: true},()=>{
         window.setTimeout(()=>{
           this.setState({saveData:false})
         },2000)
      });
   }


   onImageChange = (event) => {
      if (event.target.files && event.target.files[0]) {
         this.setState({
            dataParamImage: URL.createObjectURL(event.target.files[0])
         });

         this.setState({
            file: URL.createObjectURL(event.target.files[0])
          })
      }
   }

   render() {
      return (
         <div className="pv5">

         { this.state.missingData && <Alert type="warning">Los campos con * son necesarios</Alert> }
         { this.state.saveData && <Alert type="success">Información Guardada</Alert>}
          
         <form id="form-pay">
            <div className="flex-center">
               <p>Redeban</p>
               <div className="ma-r0">
                  <input type="hidden" 
                     name="togglePayRedeban"
                     value={this.state.checkedRb} 
                     onChange={this.handleChange}
                  />
                  <Toggle
                     checked={this.state.checkedRb}
                     onChange={e => this.setState({checkedRb: !this.state.checkedRb })}
                     name='togglePayRedeban'
                     id='rede'
                  />
               </div>
            </div>
            {this.state.checkedRb &&
               <Card>
                  <label htmlFor="valueModel">Modelo del Datáfono <span className='heavy-rebel-pink'>*</span></label>
                  <Dropdown
                     className='custom-select-model w-100 pa2'
                     placeholder="Seleccione modelo"
                     options={modelSelectItems}
                     name="valueModel"
                     value={this.state.valueModel}
                     onChange={this.handleChange}
                  />

                  <div>
                     <div>
                        <label htmlFor="dataUrl">Url</label>
                     </div>
                     <div>
                     <input type="text" id="dataUrl" 
                        name="dataUrl" 
                        value={this.state.dataUrl} 
                        onChange={this.handleChange}
                     />

                     </div>
                  </div>
                  <div>
                     <div>
                        <label htmlFor="dataTerminal">Identificación del terminal</label>
                     </div>
                     <div>
                     <input type="text" id="dataTerminal" 
                        name="dataTerminal"
                        value={this.state.dataTerminal} 
                        onChange={this.handleChange}
                     />
                     </div>
                  </div>

               </Card>
            }

            <div className="flex-center">
               <p>Mercado Pago</p>
               <div className="ma-r0">
                  <input type="hidden" 
                     name="togglePayMercado"
                     value={this.state.checkedMp} 
                     onChange={this.handleChange}
                  />
                  <Toggle
                     checked={this.state.checkedMp}
                     onChange={e => this.setState({checkedMp: !this.state.checkedMp })}
                     name='togglePayMercado'
                     id='mercado'
                  />
               </div>
            </div>
            {this.state.checkedMp &&
               <Card>
                  <div>
                     <div>
                        <label htmlFor="dataMpKey">App key</label>
                     </div>
                     <div>
                     <input type="text" id="dataMpKey" name="dataMpKey" 
                        value={this.state.dataMpKey} 
                        onChange={this.handleChange}/>
                     </div>
                  </div>
                  <div>
                     <div>
                        <label htmlFor="dataMpToken">App token </label>
                     </div>
                     <div>
                        <input type="text" id="dataMpToken" name="dataMpToken" 
                           value={this.state.dataMpToken} 
                           onChange={this.handleChange}
                        />
                     </div>
                  </div>
               </Card>
            }

            <div className="flex-center">
               <p>Scanner</p>
               <div className="ma-r0">
                  <input type="hidden" 
                     name="togglePayScanner"
                     value={this.state.checkedSc} 
                     onChange={this.handleChange}
                  />
                  <Toggle
                     checked={this.state.checkedSc}
                     onChange={e => this.setState({checkedSc: !this.state.checkedSc })}
                     name='togglePayScanner'
                     id='scanner'
                  />
               </div>
            </div>
            {this.state.checkedSc &&
               <Card>
                  <div className="mb3">
                     <input type="hidden" 
                        name="dataCheckPrint"
                        value={this.state.checkPrint} 
                        onChange={this.handleChange}
                     />
                     <Checkbox
                        checked={this.state.checkPrint}
                        label="Activar impresora"
                        name="dataCheckPrint"
                        onChange={e => this.setState({ checkPrint: !this.state.checkPrint })}
                        value="checked"
                        id="dataCheckPrint"
                     />
                  </div>

                  <div className="mt7 content-receipt">
                        <h3>Información de recibo</h3>
                        <Divider orientation="horizontal" />
                        
                        <div className="mid-gray">
                           
                           {/* <input type="file" /> */}
                           <input type="file" className="filetype" 
                              id="group_image" 
                              name="group_image" 
                              onChange={this.onImageChange} 
                           />
                           {/* <img id="target" src={this.state.dataParamImage}/>
                           <input type="hidden" 
                              name="dataParamImage"
                              value={this.state.dataParamImage} 
                              onChange={this.handleChange}
                           /> */}



                           <div>
                              <label htmlFor="dataParamTitle">Título</label>
                              <input type="text" id="dataParamTitle" name="dataParamTitle" 
                                 value={this.state.dataParamTitle} 
                                 onChange={this.handleChange}
                              />
                           </div>
                           <div>
                              <label htmlFor="dataParamNit">NIT</label>
                              <input type="text" id="dataParamNit" name="dataParamNit" 
                                 value={this.state.dataParamNit} 
                                 onChange={this.handleChange}
                              />
                           </div>

                           <div>
                              <label htmlFor="dataParamAddress">Dirección</label>
                              <input type="text" id="dataParamAddress" name="dataParamAddress" 
                                 value={this.state.dataParamAddress} 
                                 onChange={this.handleChange}
                              />
                           </div>

                           <div>
                              <label htmlFor="dataParamNameStore">Nombre de la tienda</label>
                              <input type="text" id="dataParamNameStore" name="dataParamNameStore" 
                                 value={this.state.dataParamNameStore} 
                                 onChange={this.handleChange}
                              />
                           </div>
                           
                           
                        </div>

                  </div>
               </Card>
            }

            <div className="mt5">
               <Button 
                  onClick={this.handleClick}
                  block>
                  Guardar
               </Button>
            </div>
         </form>
         </div>
      )
   }
}
