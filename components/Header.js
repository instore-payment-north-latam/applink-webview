import React, { Component } from 'react'
import Link from 'next/link'


const linkStyle = {
  marginRight: 15
}

export default class Header extends Component {

  render() {
    
    return (
      <div>
        <Link href="/settings">
          <a style={linkStyle}>Settings</a>
        </Link>
        <Link href="/">
          <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/comercios">
          <a style={linkStyle}>Comercios</a>
        </Link>
        <Link href="/scanner">
          <a style={linkStyle}>Scanner</a>
        </Link>
      </div>
    )
  }
}